﻿<%@ Page Title="Ошибка" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Planning.Pages.ErrorPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12 planningDiv pollingInProcess">
            <h2>Что-то пошло не так</h2>
            <p><label runat="server" id="CustomErrorTxt" class="errorCustomText">TEST</label></p>
            <p><i class="glyphicon glyphicon-flash"></i></p>
            <p><a class="btn btn-default" href="/Default.aspx">ВЕРНУТЬСЯ НА ДОМАШНЮЮ СТРАНИЦУ</a></p>
        </div>
    </div>
</asp:Content>
