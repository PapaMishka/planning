﻿<%@ Page Title="I этап голосования" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PollingOne.aspx.cs" Inherits="Planning.Pages.Planning.PollingOne" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">

        <div class="planning_Card pollingOne_Card">
            <h6>I ЭТАП ГОЛОСОВАНИЯ</h6>
            <p>Дата проведения мероприятия</p>
            <h2><asp:Label runat="server" ID="curPlanningDateTxt"></asp:Label></h2>
            <table>
                <caption>
                    <p>заполните следующие данные</p>
                </caption>
                <tbody>
                    <tr>
                        <th>Время мероприятия</th>
                        <td><asp:DropDownList runat="server" ID="EventDTTxt" CssClass="timeSelect">
                                <asp:ListItem Value="12:45" Text="12:00"/>
                                <asp:ListItem Value="12:45" Text="12:45" Selected="True"/>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">
                            <span>Наименование мероприятия</span>
                            <br />
                            <asp:TextBox runat="server" ID="EventsName" CssClass="eventNameInput" ></asp:TextBox>
                            <asp:TextBox ID="conEventNames" runat="server" CssClass="hidden">911|912</asp:TextBox>

                            <script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
                            <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
                            <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
                            <link href="https://code.jquery.com/ui/1.11.4/themes/start/jquery-ui.css"
                                rel="Stylesheet" type="text/css" />
                                <script src="js/bootstrap-tokenfield.js" type="text/javascript"></script>
                            <link href="css/bootstrap-tokenfield.css" rel="Stylesheet" type="text/css" />

                            <script type="text/javascript">
                                $(document).ready(function () {

                                    var namesConStr = document.getElementById("<%=conEventNames.ClientID%>").value;
                                    var namesCon = namesConStr.split("|");

                                    $("#<%=EventsName.ClientID%>").autocomplete({
                                        source: namesCon
                                    })
                                });
                            </script>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input runat="server" type="submit" value="ПРОГОЛОСОВАТЬ"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>

