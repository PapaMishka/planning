﻿<%@ Page Title="Новое голосование" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="newPlanning.aspx.cs" Inherits="Planning.Pages.Planning.newPlanning" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="planning_Card newPlanning_AddCard">
            <h6>НОВОЕ ГОЛОСОВАНИЕ</h6>
            <p>Дата проведения мероприятия</p>
            <h2><asp:Label runat="server" ID="curPlanningDateTxt"></asp:Label></h2>
            <table>
                <caption>
                    <p>заполните следующие данные</p>
                </caption>
                <tbody>
                    <tr>
                        <th>Продолжительность I этапа, мин</th>
                        <td><input runat="server" type="number" value="60" id="pollingOneTxt" /></td>
                    </tr>
                    <tr>
                        <th>Продолжительность II этапа, мин</th>
                        <td><input runat="server" type="number" value="10" id="pollingTwoTxt" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input runat="server" type="submit" value="СОЗДАТЬ ГОЛОСОВАНИЕ"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
