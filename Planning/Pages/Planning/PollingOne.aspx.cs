﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Services;
using System.Web.Script.Services;

using System.Data;
using Planning.Controllers;

namespace Planning.Pages.Planning
{
    public partial class PollingOne : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            curPlanningDateTxt.Text = Default.getStartedDT().ToShortDateString();

            if (IsPostBack)
            {
                createNewPolling();
            }
            else
            {
                initEventTimeList();
                initEventNamesCon();
            }
        }


        public void initEventNamesCon()
        {
            //List<string> eventNames = new List<string>();
            string tableName = "MyPlannings_Events"
                  , query = "select EventName_RU from [" + tableName + "] group by EventName_RU";

            DataTable dt = DBUtils.getDataTable(query, tableName);

            conEventNames.Text = "";
            var idx = 0;
            while (idx < dt.Rows.Count)
            {
                if (conEventNames.Text != "")
                    conEventNames.Text += "|";
                conEventNames.Text += dt.Rows[idx]["EventName_RU"].ToString().Trim();
                //eventNames.Add(dt.Rows[idx]["EventName_RU"].ToString());
                idx++;
            }
        }

        public void initEventTimeList()
        {
            EventDTTxt.Items.Clear();

            ListItem item;
            if (EventDTTxt.Items.Count == 0)
            {
                DateTime start = DateTime.Now;
                DateTime begin = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                DateTime end = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 23:00:00"));
                var span = new TimeSpan(0, 15, 0);

                for (var i = begin; i <= end; i += span)
                {
                    if (i >= start)
                    {
                        string text = i.ToString("HH:mm");
                        string value = i.ToString("HH:mm");

                        item = new ListItem(text, value);
                        EventDTTxt.Items.Add(item);
                    }
                }
                EventDTTxt.Items[0].Selected = true;
            }
        }

        protected void createNewPolling()
        {
            string tableName = "myPlannings_Events";
            string[] fieldList = { "EventDT"
                                 , "EventName_RU"
                                 , "PID"
                                 , "CreatedBy"
                                 };

            string dtstr = Default.getStartedDT().ToString("yyyy-MM-dd") + " " + EventDTTxt.SelectedValue + ":00";

            string[] valueList = { "'" + dtstr + "'"
                                 , "N'" + EventsName.Text + "'"
                                 , Default.getStartedPlanningId().ToString()
                                 , "'" + Default.getUserIdByName(User.Identity.Name) + "'"
                                 };
            DBUtils.addNewRec(tableName, fieldList, valueList);
            Response.Redirect("/Default.aspx");
        }
    }
}