﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Planning.Controllers;

namespace Planning.Pages.Planning
{
    public partial class newPlanning : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            curPlanningDateTxt.Text = DateTime.Now.ToShortDateString();
            if (IsPostBack)
            {
                createNewPlanning();
            }
        }

        protected void createNewPlanning()
        {
            string tableName = "myPlannings";
            string[] fieldList = { "PlanningDT"
                                 , "PollingOneInMinutes"
                                 , "PollingTwoInMinutes"
                                 , "Status"
                                 , "CreatedBy"
                                 };
            string[] valueList = { "'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                                 , pollingOneTxt.Value
                                 , pollingTwoTxt.Value
                                 , Default.getStartedStatusId().ToString()
                                 , "'" + Default.getUserIdByName(User.Identity.Name) + "'"
                                 };
            if (!Default.startedPlanningIsExist())
                DBUtils.addNewRec(tableName, fieldList, valueList);
            else
                Response.Redirect("/Pages/ErrorPage.aspx?ErrorId=1");
            Response.Redirect("/Default.aspx");
        }
    }
}