﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Planning.Controllers;

namespace Planning.Pages
{
    public partial class ErrorPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request["ErrorId"] != null)
            {
                CustomErrorTxt.InnerText = Errors.getCustomErrorMessage(Convert.ToInt32(Request["ErrorId"]));
            }
        }
    }
}