﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Planning.Controllers;

namespace Planning
{
    public partial class _Default : Page
    {
        public static string Delimiter = "|";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.Name != string.Empty)
            {
                initForAuthorized();
            }
            else
            {
                initUnAuthorized();
            }

        }

        public void initUnAuthorized()
        {
            unAuthorized.Visible = true;
        }

        public void initForAuthorized()
        {
            if (!Default.startedPlanningIsExist())
            {
                initStartPlanningElements();
            }
            else if ((Default.getCurrentPollingNo() == 1 || Default.getCurrentPollingNo() == 2)
                      || Default.lastCompletedPlanningIsExist()
                    )
            {
                initPollingElements();
            }
        }


        public void initStartPlanningElements()
        {
            newPlanningDiv.Visible = true;
            pollingOneDiv.Visible = false;

            if (Default.lastCompletedPlanningIsExist())
            {
                pollingInProcessDiv.Visible = true;
                labelStatus.Text = "ЗАВЕРШЕН II";
                labelDate.Text = Default.getCompletedDT().ToShortDateString();
                PollingOneTable.Visible = false;
                pollingTwoTable.Visible = true;

                winEventName.Text = Default.getWinEventName();
                winEventTime.Text = Default.getWinEventDT().ToString("HH:mm");

                JoinToPartyDialog.Visible = false;
                pollingInProcess_settings.Visible = false;
            }
            else
                pollingInProcessDiv.Visible = false;
        }



        public void initPollingElements()
        {
            if (Default.getCurrentPollingNo() == 1)
            {
                labelStatus.Text = "ПРОВОДИТСЯ I";
                labelDate.Text = Default.getStartedDT().ToShortDateString();
                PollingOneTable.Visible = true;
                pollingTwoTable.Visible = false;

                labelTimeLeft.Text = Default.getTimeLeft();

                JoinToPartyDialog.Visible = false;
            }
            else if (Default.getCurrentPollingNo() == 2)
            {
                labelStatus.Text = "ПРОВОДИТСЯ II";
                labelDate.Text = Default.getStartedDT().ToShortDateString();
                PollingOneTable.Visible = false;
                pollingTwoTable.Visible = true;

                winEventName.Text = Default.getWinEventName();
                winEventTime.Text = Default.getWinEventDT().ToString("HH:mm");

                pollingInProcess_settings.Visible = Default.isPlanningCreator(User.Identity.Name);

                JoinToPartyDialog.Visible = !Default.userExistInPartyList(User.Identity.Name) && !Default.userWasSurved(User.Identity.Name);
            }

            labelPollCnt.Text = Default.getTotalPolledCnt().ToString();

            newPlanningDiv.Visible = false;
            if (Default.getCurrentPollingNo() == 1 && !Default.curUserIsPolledAlready(User.Identity.Name))
                pollingOneDiv.Visible = true;
            else
                pollingOneDiv.Visible = false;
            pollingInProcessDiv.Visible = true;
        }

        public void cancelPlanning_Click(object sender, EventArgs e)
        {
            string tableName = "MyPlannings"
                  , whereValue = "status = " + Default.getStartedStatusId()
                  ;
            string[] fieldList = new string[] { "status" };
            string[] valueList = new string[] { Default.getCanceledStatusId().ToString() };
            DBUtils.editRec(tableName, fieldList, valueList, whereValue);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void joinToParty_OK_Click(object sender, EventArgs e)
        {
            string tableName = "MyPlannings_Events";
            string[] fieldList = new string[] { "EventDT"
                                              , "EventName_RU" 
                                              , "PID" 
                                              , "CreatedBy" 
                                              , "wasAddedFromSurvey" 
                                              };
            string[] valueList = new string[] { "'" + Default.getWinEventDT().ToString("yyyy-MM-dd HH:mm:ss") + "'"
                                              , "N'" + Default.getWinEventName() + "'"
                                              , Default.getStartedPlanningId().ToString()
                                              , "'" + Default.getUserIdByName(User.Identity.Name) + "'"
                                              , "1"
                                              };
            DBUtils.addNewRec(tableName, fieldList, valueList);

            tableName = "MyPlannings_Survey";
            fieldList = new string[] { "pid"
                                     , "userId" 
                                     };
            valueList = new string[] { Default.getStartedPlanningId().ToString()
                                     , "'" + Default.getUserIdByName(User.Identity.Name) + "'"
                                     };
            DBUtils.addNewRec(tableName, fieldList, valueList);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void joinToParty_CANCEL_Click(object sender, EventArgs e)
        {
            string tableName = "MyPlannings_Survey";
            string[] fieldList = new string[] { "pid"
                                              , "userId" 
                                              };
            string[] valueList = new string[] { Default.getStartedPlanningId().ToString()
                                              , "'" + Default.getUserIdByName(User.Identity.Name) + "'"
                                              };
            DBUtils.addNewRec(tableName, fieldList, valueList);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }
}