﻿<%@ Page Title="Домашняя" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Planning._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div runat="server" id="unAuthorized" class="col-md-12 planningDiv pollingOne" visible="false">
            <h4>ДОБРО ПОЖАЛОВАТЬ НА ТЕСТОВЫЙ САЙТ</h4>
            <h5>проведения голосований по списку проводимых мероприятий</h5>
            <h2>МЕРОПРИЯТИЯ.РФ</h2>
            <p><i class="glyphicon glyphicon-bullhorn"></i></p>

            <h6>Будте как дома :)</h6>
        </div>

        <div runat="server" id="newPlanningDiv" class="col-md-12 planningDiv newPlanning" visible="false">
            <h2>Доступно новое голосование!</h2>
            <p class="centered">Запустите новое голосование и спланируйте список новых мероприятий на сегодня.</p>
            <p><i class="glyphicon glyphicon-bullhorn"></i></p>
            <p><a class="btn btn-default" href="/Pages/Planning/newPlanning.aspx">НАЧАТЬ НОВОЕ ГОЛОСОВАНИЕ</a></p>
        </div>

        <div runat="server" id="pollingOneDiv" class="col-md-12 planningDiv pollingOne" visible="false">
            <h2>Запущен 1 этап голосования на проведение мероприятий!</h2>
            <p class="centered">Оставьте и Вы свой голос и пусть он станет решающим!</p>
            <p><i class="glyphicon glyphicon-thumbs-up"></i></p>
            <p><a class="btn btn-default" href="/Pages/Planning/pollingOne.aspx">ПРОГОЛОСОВАТЬ</a></p>
        </div>

        <div runat="server" id="pollingInProcessDiv" class="col-md-12 planningDiv pollingInProcess" visible="false">
           <div id="pollingInProcess_settings" runat="server">
              <div class="dropdown settingsBtnCon">
                <button class="settingsBtn dropdown-toggle" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></button>
                <ul class="dropdown-menu dropdownRight">
                    <li><asp:LinkButton ID="cancelPlanning" runat="server" OnClick="cancelPlanning_Click">Отменить голосование</asp:LinkButton></li>
                </ul>
              </div>
           </div>  

            <h4><asp:Label ID="labelStatus" runat="server"></asp:Label> ЭТАП</h4>
            <h5> голосования по списку мероприятий на</h5>
            <h2><asp:Label ID="labelDate" runat="server"></asp:Label></h2>

            <div id="PollingOneTable" runat="server" class="curPlanningInfo">
                <div class="col-md-6">
                    <div class="statCaption">статистика голосования</div>
                    <div class="statSimpleTable">
                        <table>
                            <tr>
                                <th>до окончания I этапа голосования:</th>
                                <td><asp:Label ID="labelTimeLeft" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <th>Всего проголосовало:</th>
                                <td><asp:Label ID="labelPollCnt" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                        <script type="text/javascript">
                            var PollingOneTable = document.getElementById('MainContent_PollingOneTable');
                            if (PollingOneTable != null)
                            {
                                var myInterval = setInterval(myTimer, 1000);
                                function myTimer() {
                                    getTimeLeft();
                                }
                            }
                        </script>
                    </div>
                </div>
            </div>


            <div id="pollingTwoTable" runat="server" class="curPlanningInfo">
                <div class="col-md-6">
                    <div class="statCaption">Результаты голосования</div>
                    <div class="statSimpleTable">
                        <table>
                            <tr>
                                <th>Мероприятие-победитель:</th>
                                <td><asp:Label ID="winEventName" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <th>Время проведения:</th>
                                <td><asp:Label ID="winEventTime" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </div>
                    <div class="statCaption">Данные, получаемые через Comet-технологии</div>
                    <div class="statSimpleTable">
                        <table>
                            <tr>
                                <th>Время сервера:</th>
                                <td><span id="outputZone"></span><asp:Label ID="label1" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                        <script src="/Scripts/Comet_PushMessages.js"></script>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                getData();
                            });
                        </script>
                    </div>
                    <div class="statCaption">Список участников мероприятия</div>
                    <div>
                        <ul ID="partyList" Class="partyList">
                        </ul>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="uberCoolChart"></div>

                    <script type="text/javascript">
                        var uberCoolChart = document.getElementById('uberCoolChart');
                        if (uberCoolChart != null) {
                            getPartyList();
                            UberCoolChart_generate();
                        }


                        var partyList = document.getElementById('partyList');
                        if (partyList != null) {
                            var myInterval = setInterval(myTimer, 3000);
                            function myTimer() {
                                getPartyList();
                            }
                        }
                    </script>
                </div>
                
            </div>
            <div id="JoinToPartyDialog" runat="server" style="background-color: #fff;color: #807363; padding: 5px 5px 15px 5px;">
                <h4>Успейте и Вы принять участие в победившем мероприятии</h4>
                <div class="pollingInProcessBtnCon">
                    <asp:LinkButton ID="joinToParty_OK" runat="server" cssClass="pollingInProcessBtn" OnClick="joinToParty_OK_Click">ПРИСОЕДИНИТЬСЯ</asp:LinkButton>
                    <asp:LinkButton ID="joinToParty_CANCEL" runat="server" CssClass="pollingInProcessBtn" OnClick="joinToParty_CANCEL_Click">ОТКАЗАТЬСЯ</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
