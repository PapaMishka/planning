﻿function getTimeLeft() {
    var url = '/Default.asmx/getTimeLeft';
    $.ajax({
        type: 'POST',
        url: url,
        //data: '{  }',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var con = response.d;

            if (con[0] == "00:00:00")
                location.reload();

            var labelTimeLeft = document.getElementById('MainContent_labelTimeLeft');
            if (labelTimeLeft)
                labelTimeLeft.innerText = con[0];

            var labelPollCnt = document.getElementById('MainContent_labelPollCnt');
            if (labelPollCnt)
                labelPollCnt.innerText = con[1];

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function getPartyList() {
    var url = '/Default.asmx/getPartyList';
    $.ajax({
        type: 'POST',
        url: url,
        //data: '{  }',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var partyList = document.getElementById('partyList');
            if (partyList) {
                var lis = partyList.getElementsByTagName('li');
                while (lis.length > 0) {
                    lis[0].parentNode.removeChild(lis[0]);
                }
            }

            eval(response.d);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}