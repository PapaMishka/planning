﻿function UberCoolChart_generate() {
    var url = '/Default.asmx/getChartData';
    $.ajax({
        type: 'POST',
        url: url,
        //data: '{  }',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (response) {
            var colors = Highcharts.getOptions().colors,
                categories,
                data;

            eval(response.d);

            UberCoolChart_BuildChart(categories, data);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function UberCoolChart_BuildChart(categories, data) {
    var browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;

    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get()
            });
        }
    }

    // Create the chart
    Highcharts.chart('uberCoolChart', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Результаты II этапа голосования'
        },
        subtitle: {
            text: 'по списку мероприятий'
        },
        yAxis: {
            title: {
                text: 'Голоса'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            }
        },
        tooltip: {
            valueSuffix: ' гол.'
        },
        series: [{
            name: 'Время',
            data: browserData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'transparent',
                distance: -30
            }
        }, {
            name: 'Мероприятие',
            data: versionsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
                        this.y + ' гол.' : null;
                }
            },
            id: 'versions'
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 400
                },
                chartOptions: {
                    series: [{
                        id: 'versions',
                        dataLabels: {
                            enabled: false
                        }
                    }]
                }
            }]
        }
    });
}