﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Configuration;

using Planning.Controllers;

namespace Planning.Controllers
{
    public class Default : Controller
    {
        public static string conStr = ConfigurationManager.AppSettings["PlanningCS"];

        public static void setPlanningCompleted()
        {
            string tableName = "MyPlannings"
                  , whereValue = "status = " + Default.getStartedStatusId()
                  ;
            string[] fieldList = new string[] { "status" };
            string[] valueList = new string[] { Default.getCompletedStatusId().ToString() };
            DBUtils.editRec(tableName, fieldList, valueList, whereValue);
        }

        public static int getCanceledStatusId()
        {
            string tableName = "PlanningStatuses"
                  , query = "select id from [" + tableName + "] where Name = 'Canceled';"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (int)dt.Rows[0][0];

            throw new Exception("Ошибка! В таблице PlanningStatuses для тек. голосования не найден статус 'Completed'!");
        }



        public static Boolean userWasSurved(string _userName)
        {
            string tableName = "MyPlannings_Survey"
                  , query = "select Id from [MyPlannings_Survey]"
                          + " where pid = " + getStartedPlanningId().ToString()
                          + "   and userId = '" + getUserIdByName(_userName) + "'"
                          + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return true;

            return false;
        }

        public static Boolean userExistInPartyList(string _userName)
        {
            int status = Default.getStartedStatusId();

            string query = "select UserName, EventName_RU from [AspNetUsers]"
                         + "  join [MyPlannings_Events] on AspNetUsers.id = MyPlannings_Events.createdBy "
                         + "  join [MyPlannings] on MyPlannings.id = MyPlannings_Events.pid "
                         + " where MyPlannings.status = " + status
                         + "   and MyPlannings_Events.EventDT = '" + Default.getWinEventDT().ToString("yyyy-MM-dd HH:mm:ss") + "'"
                         ;

            DataTable dt = DBUtils.getDataTable(query, "AspNetUsers");

            var idx = 0;
            while (idx < dt.Rows.Count)
            {
                string EventName_RU = dt.Rows[idx][1].ToString().Trim();
                if (EventName_RU == Default.getWinEventName()) //сорян за костыль: рамс с кодировкой. Разбираться некогда
                {
                    string partyUser = dt.Rows[idx][0].ToString().Trim();
                    if (partyUser == _userName)
                        return true;
                }
                idx++;
            }
            return false;
        }


        public static Boolean isPlanningCreator(string _userName)
        {
            string tableName = "MyPlannings"
                  , query = "select id from [" + tableName + "]"
                          + " where Status = " + getStartedStatusId()
                          + "   and CreatedBy = '" + getUserIdByName(_userName) + "'"
                          + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return true;

            return false;
        }

        public static string getTimeLeft()
        {
            string tableName = "MyPlannings"
                  , query = "select PlanningDT, PollingOneInMinutes, PollingTwoInMinutes from [MyPlannings] where Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
            {
                DateTime PlanningDT = (DateTime)dt.Rows[0][0];
                int PollingOneInMinutes = (int)dt.Rows[0][1];
                int PollingTwoInMinutes = (int)dt.Rows[0][2];

                if (DateTime.Now <= PlanningDT.AddMinutes(PollingOneInMinutes))
                {
                    PlanningDT = PlanningDT.AddMinutes(PollingOneInMinutes);
                    TimeSpan timeLeft = PlanningDT - DateTime.Now;
                    string res = new DateTime(timeLeft.Ticks).ToString("HH:mm:ss");
                    return res;
                }
                else if (DateTime.Now <= PlanningDT.AddMinutes(PollingOneInMinutes + PollingTwoInMinutes))
                {
                    PlanningDT = PlanningDT.AddMinutes(PollingOneInMinutes + PollingTwoInMinutes);
                    TimeSpan timeLeft = PlanningDT - DateTime.Now;
                    string res = new DateTime(timeLeft.Ticks).ToString("HH:mm:ss");
                    return res;
                }
                else
                {
                    setPlanningCompleted();
                }
            }

            return "00:00:00";
        }

        public static string getWinEventName()
        {
            int curStatus = 0;
            if (getCurrentPollingNo() == 2)
                curStatus = getStartedStatusId();
            else
                curStatus = getCompletedStatusId();

            string tableName = "MyPlannings_Events"
                  , childTableName = "MyPlannings"
                  , query = "select PlanningDT, Count(" + tableName + ".id), EventName_RU, EventDT from [" + tableName + "]"
                          + "  join [" + childTableName + "] on " + childTableName + ".id = " + tableName + ".PID"
                          + " where " + childTableName + ".status = " + curStatus
                          + " group by PlanningDT, EventName_RU, EventDT"
                          + " order by PlanningDT desc, Count(" + tableName + ".id) desc"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return dt.Rows[0]["EventName_RU"].ToString().Trim();

            throw new Exception("Ошибка! В таблице MyPlannings не найдено искомое голосование!");
        }
        public static DateTime getWinEventDT()
        {
            int curStatus = 0;
            if (getCurrentPollingNo() == 2)
                curStatus = getStartedStatusId();
            else
                curStatus = getCompletedStatusId();

            string tableName = "MyPlannings_Events"
                  , childTableName = "MyPlannings"
                  , query = "select PlanningDT, Count(" + tableName + ".id), EventName_RU, EventDT from [" + tableName + "]"
                          + "  join [" + childTableName + "] on " + childTableName + ".id = " + tableName + ".PID"
                          + " where " + childTableName + ".status = " + curStatus
                          + " group by PlanningDT, EventName_RU, EventDT"
                          + " order by PlanningDT desc, Count(" + tableName + ".id) desc"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return ((DateTime)dt.Rows[0]["EventDT"]);

            throw new Exception("Ошибка! В таблице MyPlannings не найдено искомое голосование!");
        }




        public static string getStartedUserName()
        {
            string tableName = "MyPlannings"
                  , query = "select UserName from [AspNetUsers] join [" + tableName + "] on AspNetUsers.id = " + tableName + ".CreatedBy and Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return dt.Rows[0][0].ToString();

            throw new Exception("Ошибка! В таблице PlanningDT не найдено голосование со статусом 'Started' или пользователь, создавший его уже удален!");
        }

        public static DateTime getCompletedDT()
        {
            string tableName = "MyPlannings"
                  , query = "select PlanningDT from [" + tableName + "] where Status = " + getCompletedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (DateTime)dt.Rows[0][0];

            throw new Exception("Ошибка! В таблице PlanningDT не найдено голосование со статусом 'Started'!");
        }

        public static int getTotalPolledCnt()
        {
            string tableName = "MyPlannings"
                  , query = "select Count(MyPlannings_Events.Id) from [" + tableName + "] join [MyPlannings_Events] on " + tableName + ".id = MyPlannings_Events.pid where Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (int)dt.Rows[0][0];

            throw new Exception("Ошибка! В таблице PlanningDT не найдено голосование со статусом 'Started'!");
        }


        public static DateTime getStartedDT()
        {
            string tableName = "MyPlannings"
                  , query = "select PlanningDT from [" + tableName + "] where Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (DateTime)dt.Rows[0][0];

            throw new Exception("Ошибка! В таблице PlanningDT не найдено голосование со статусом 'Started'!");
        }

        public static int getStartedStatusId()
        {
            string tableName = "PlanningStatuses"
                  , query = "select id from [" + tableName + "] where Name = 'Started';"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (int)dt.Rows[0][0];

            throw new Exception("Ошибка! В таблице PlanningStatuses для тек. голосования не найден статус 'Started'!");
        }
        public static int getCompletedStatusId()
        {
            string tableName = "PlanningStatuses"
                  , query = "select id from [" + tableName + "] where Name = 'Completed';"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (int)dt.Rows[0][0];

            throw new Exception("Ошибка! В таблице PlanningStatuses для тек. голосования не найден статус 'Completed'!");
        }

        //id пользователя по имени
        public static string getUserIdByName(string _userName)
        {
            string tableName = "AspNetUsers"
                  , query = "select id from [" + tableName + "] where UserName = '" + _userName + "';"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return dt.Rows[0][0].ToString();

            return null;
        }

        //есть ли сейчас активные голосования
        public static Boolean startedPlanningIsExist()
        {
            string tableName = "MyPlannings"
                  , query = "select id from [" + tableName + "] where Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return true;

            return false;
        }
        //есть ли сейчас завершенные голосования
        public static Boolean lastCompletedPlanningIsExist()
        {
            string tableName = "MyPlannings"
                  , query = "select id from [" + tableName + "] where Status = " + getCompletedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return true;

            return false;
        }

        //номер этапа активного голосования
        public static int getCurrentPollingNo()
        {
            int res = 0;

            string tableName = "MyPlannings"
                  , query = "select PlanningDT, PollingOneInMinutes, PollingTwoInMinutes from [" + tableName + "] where Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
            {
                DateTime planningDT = (DateTime)dt.Rows[0][0];
                int pollingOne = (int)dt.Rows[0][1];
                int pollingTwo = (int)dt.Rows[0][2];
                if (DateTime.Now <= planningDT.AddMinutes(pollingOne))
                    res = 1;
                else if (DateTime.Now <= planningDT.AddMinutes(pollingOne + pollingTwo))
                    res = 2;
            }

            return res;
        }

        //id начатого голосования
        public static int getStartedPlanningId()
        {
            string tableName = "MyPlannings"
                  , query = "select id from [" + tableName + "] where Status = " + getStartedStatusId() + ";"
                  ;

            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return (int)dt.Rows[0][0];

            return 0;
        }

        //тек. пользователь уже проголосовал
        public static Boolean curUserIsPolledAlready(string _userName)
        {
            string tableName = "MyPlannings_Events"
                  , query = "select id from [" + tableName + "]"
                  + " where PID = " + getStartedPlanningId()
                  + " and CreatedBy = '" + getUserIdByName(_userName) + "'"
                  + ";"
                  ;
            DataTable dt = DBUtils.getDataTable(query, tableName);

            if (dt.Rows.Count > 0)
                return true;

            return false;
        }
	}
}