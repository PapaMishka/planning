﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Planning.Controllers
{
    public class DBUtils : Controller
    {
        public static void executeCmd(SqlCommand cmd)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[Default.conStr].ConnectionString))
            {
                String strConnString = System.Configuration.ConfigurationManager
                .ConnectionStrings[Default.conStr].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    con.Close();
                    con.Dispose();
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public static void addNewRec(string _tableName, string[] _fieldList, string[] _valueList)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[Default.conStr].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                var fieldList = "";
                for (var i = 0; i < _fieldList.Length; i++)
                {
                    if (i > 0)
                    {
                        fieldList += ", ";
                    }
                    fieldList += "[" + _fieldList[i] + "]";
                }

                var valueList = "";
                for (var i = 0; i < _valueList.Length; i++)
                {
                    if (i > 0)
                    {
                        valueList += ", ";
                    }
                    valueList += _valueList[i];
                }

                cmd.CommandText = " INSERT INTO [" + _tableName + "] (" + fieldList + ") VALUES(" + valueList + ")";
                cmd.ExecuteNonQuery();
            }
        }
        public static void editRec(string _tableName, string[] _fieldList, string[] _valueList, string _whereValue = "")
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[Default.conStr].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                var setList = "";
                for (var i = 0; i < _fieldList.Length; i++)
                {
                    if (i > 0)
                    {
                        setList += ", ";
                    }
                    setList += "[" + _fieldList[i] + "]=" + _valueList[i];
                }

                cmd.CommandText = " UPDATE [" + _tableName + "] SET " + setList + " where " + _whereValue;
                cmd.ExecuteNonQuery();
            }
        }
        public static void delRec(string _tableName, string _whereValue = "")
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[Default.conStr].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                cmd.CommandText = " DELETE FROM [" + _tableName + "] where " + _whereValue;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable getDataTable(string _query, string _resTableName)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[Default.conStr].ConnectionString))
            {
                SqlDataAdapter sqlDa = new SqlDataAdapter(_query, con);

                DataSet dSet = new DataSet();

                sqlDa.Fill(dSet, _resTableName);

                return dSet.Tables[_resTableName];
            }
        }
	}
}