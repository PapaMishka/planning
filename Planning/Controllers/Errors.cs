﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Planning.Controllers;

namespace Planning.Controllers
{
    public class Errors : Controller
    {
        public static string getCustomErrorMessage(int _id)
        {
            switch (_id)
            {
                case 1:
                    return "Не удалось создать новое голосование: пользователь " + Default.getStartedUserName().ToUpper() + " успел создать его раньше.";
                default:
                    return string.Empty;
            }
        }
	}
}