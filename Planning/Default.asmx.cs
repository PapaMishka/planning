﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Data;
using Planning.Controllers;

namespace Planning.WebServices
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        [WebMethod]
        public string getPartyList()
        {
            if (Default.getTimeLeft() == "00:00:00" && Default.getStartedPlanningId() != 0)
                return "location.reload();";

            string res = " var partyList = document.getElementById('partyList');";

            int status = 0;
            if (Default.getCurrentPollingNo() == 2)
                status = Default.getStartedStatusId();
            else
                status = Default.getCompletedStatusId();

            string query = "select UserName, EventName_RU from [AspNetUsers]"
                         + "  join [MyPlannings_Events] on AspNetUsers.id = MyPlannings_Events.createdBy "
                         + "  join [MyPlannings] on MyPlannings.id = MyPlannings_Events.pid "
                         + " where MyPlannings.status = " + status
                         + "   and MyPlannings_Events.EventDT = '" + Default.getWinEventDT().ToString("yyyy-MM-dd HH:mm:ss") + "'"
                         ;

            DataTable dt = DBUtils.getDataTable(query, "AspNetUsers");

            var idx = 0;
            while (idx < dt.Rows.Count)
            {
                string EventName_RU = dt.Rows[idx][1].ToString().Trim();
                if (EventName_RU == Default.getWinEventName()) //сорян за костыль: рамс с кодировкой. Разбираться некогда
                {
                    string PartyItem = dt.Rows[idx][0].ToString().Trim();
                    res += " var li = document.createElement('li');";
                    res += "     li.innerHTML = '" + PartyItem + "';";
                    res += " partyList.appendChild(li);";
                }
                idx++;
            }


            return res;
        }




        [WebMethod]
        public string[] getTimeLeft()
        {
            string timeLeft = Default.getTimeLeft()
                 , polledCnt = Default.getTotalPolledCnt().ToString();


            return new string[] { timeLeft, polledCnt };
        }
        

        [WebMethod]
        public string getChartData()
        {
            string categories = " categories = [";
            string data = " data = [{";

            int status = 0;
            if (Default.getCurrentPollingNo() == 2)
                status = Default.getStartedStatusId();
            else if (Default.lastCompletedPlanningIsExist())
                status = Default.getCompletedStatusId();

            //Голосования
            string tnMyPlannings = "MyPlannings"
                 , qMyPlannings = "select * from [" + tnMyPlannings + "] where status = " + status + " order By Id desc"
            ;
            DataTable dtMyPlannings = DBUtils.getDataTable(qMyPlannings, tnMyPlannings);
            var idxMyPlannings = 0;

            while (idxMyPlannings < 1)
            {
                //Времена событий
                string tnMyPlannings_Times = "MyPlannings_Events"
                     , qMyPlannings_Times = "select EventDT from [" + tnMyPlannings_Times + "]"
                                           + " where pid = " + dtMyPlannings.Rows[idxMyPlannings]["id"].ToString()
                                           + "   and (wasAddedFromSurvey = 0 or wasAddedFromSurvey is null)"
                                           + " GROUP BY EventDT"
                                           + " ORDER BY EventDT"
                                           ;
                ;
                DataTable dtMyPlannings_Times = DBUtils.getDataTable(qMyPlannings_Times, tnMyPlannings_Times);


                var idxMyPlannings_Times = 0;
                while (idxMyPlannings_Times < dtMyPlannings_Times.Rows.Count)
                {
                    string tst = dtMyPlannings_Times.Rows[idxMyPlannings_Times]["EventDT"].ToString();
                    DateTime EventDT = (DateTime)dtMyPlannings_Times.Rows[idxMyPlannings_Times]["EventDT"];
                    //Добавляем время в массив категорий
                    if (idxMyPlannings_Times > 0)
                    {
                        categories += ", ";
                        data += " }, {";

                    }
                    categories += "'" + EventDT.ToString("HH:mm") + "'";

                    //Наименования событий и кол-во проголосовавших за них лиц
                    int votesCnt = 0;

                    string tnMyPlannings_Events = "MyPlannings_Events"
                         , qMyPlannings_Events = "select EventName_RU, count(id) from [" + tnMyPlannings_Events + "]"
                                               + " where pid = " + dtMyPlannings.Rows[idxMyPlannings]["id"].ToString()
                                               + "   and EventDT = '" + EventDT.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                                               + "   and (wasAddedFromSurvey = 0 or wasAddedFromSurvey is null)"
                                               + " GROUP BY EventName_RU"
                                               + " ORDER BY EventName_RU"
                                               ;
                    ;

                    DataTable dtMyPlannings_Events = DBUtils.getDataTable(qMyPlannings_Events, tnMyPlannings_Events);
                    string EventNames = string.Empty;
                    string EventValues = string.Empty;
                    var idxMyPlannings_Events = 0;
                    while (idxMyPlannings_Events < dtMyPlannings_Events.Rows.Count)
                    {
                        //Заполняем контейнер 
                        if (idxMyPlannings_Events > 0)
                        {
                            EventNames += ", ";
                            EventValues += ", ";
                        }
                        EventNames += "'" + dtMyPlannings_Events.Rows[idxMyPlannings_Events]["EventName_RU"].ToString().Trim() + "'";
                        EventValues += dtMyPlannings_Events.Rows[idxMyPlannings_Events][1].ToString();

                        votesCnt += (int)dtMyPlannings_Events.Rows[idxMyPlannings_Events][1];

                        idxMyPlannings_Events++;
                    }

                    data += "    y: " + votesCnt + ",";
                    data += "    color: colors[" + idxMyPlannings_Times + "],";
                    data += "    drilldown: {";
                    data += "        name: 'Event" + idxMyPlannings_Times + "',";
                    data += "        categories: [" + EventNames + "],";
                    data += "        data: [" + EventValues + "],";
                    data += "        color: colors[" + idxMyPlannings_Times + "]";
                    data += "    }";



                    idxMyPlannings_Times++;
                }
                categories += "];";
                data += " }];";

                idxMyPlannings++;
            }
             

            string res = string.Empty;

            res += categories;

            res += data;

            /*
            res += "    y: 2,";
            res += "    color: colors[0],";
            res += "    drilldown: {";
            res += "        name: 'Event01',";
            res += "        categories: ['Тестовое мероприятие 1'],";
            res += "        data: [2],";
            res += "        color: colors[0]";
            res += "    }";

            res += " }, {";

            res += "     y: 1,";
            res += "     color: colors[2],";
            res += "     drilldown: {";
            res += "         name: 'Event02',";
            res += "         categories: ['ТЕСТ03'],";
            res += "         data: [1],";
            res += "         color: colors[1]";
            res += "     }";
             * */

            return res;
        }
    }
}
