﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Planning.Controllers;

namespace Planning
{
    public partial class CometRulez : System.Web.UI.Page
    {
        public static string Delimiter = "|";

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Buffer = false;

            while (true)
            {
                Response.Write(Delimiter
                    + DateTime.Now.ToString("HH:mm:ss.FFF"));
                Response.Flush();

                Default.getTimeLeft();

                // Suspend the thread for 1/2 a second
                System.Threading.Thread.Sleep(1000);
            }

            // Yes I know we'll never get here,
            // it's just hard not to include it!
            Response.End();
        }
    }
}